<!DOCTYPE html>
<html>
<head>
	<title>Assignment 5</title>
	<style type="text/css">
	* {
		font-family: "Arial";
	}
	</style>
</head>
<body>
<h3>Assignment 5</h3>
<?php 
	
	class SkierLog {
		protected $doc = null;
		protected $x = null;

		public function __construct() {
			try {
				$this->doc = new DOMDocument();
				if (!$this->doc->load("./SkierLogs.xml")) {
					die("<p style='color:red;'>Can't load XML file.</p>");
				}
				$this->x = new DOMXPath($this->doc);
				$this->x->query("//Clubs/Club");
				//echo $doc->saveXML(); // DEBUG
				//echo 'Loaded XML.';
			} catch (Exception $e) {
				die("<p style='color:red;'>Can't load XML file: " . $e->getMessage() . "</p>");
			}
		}

		public function getClubs() {
			$array = array();
			$x = $this->x;

			foreach ($x->query("//Clubs/Club") as $item) {
				$id = $item->getAttribute("id");
				$name = $x->query("./Name", $item)->item(0)->textContent;
				$city = $x->query("./City", $item)->item(0)->textContent;
				$county = $x->query("./County", $item)->item(0)->textContent;

				$club = array(
					"id"=>$id,
					"name"=>$name,
					"city"=>$city,
					"county"=>$county
				);

				array_push($array, $club);
			}

			return $array;
		}

		public function getSkiers() {
			$array = array();
			$x = $this->x;

			foreach ($x->query("/SkierLogs/Skiers/Skier") as $item) {
				$userName = $item->getAttribute("userName");
				$firstName = $x->query("./FirstName", $item)->item(0)->textContent;
				$lastName = $x->query("./LastName", $item)->item(0)->textContent;
				$yearOfBirth = $x->query("./YearOfBirth", $item)->item(0)->textContent;

				$skier = array(
					"userName"=>$userName,
					"firstName"=>$firstName,
					"lastName"=>$lastName,
					"yearOfBirth"=>$yearOfBirth
				);

				array_push($array, $skier);
			}

			return $array;
		}

		public function getClubFromUsername($season, $userName) {
			$array = array();
			$x = $this->x;

			$q = "//Season[@fallYear='".$season."']/Skiers[./Skier[@userName='".$userName."']]/@clubId";
			$res = $x->query($q);
			if ($res->length == 0) {
				return null;
			}

			return $res->item(0)->textContent;
		}

		public function getTotalDistance($season, $userName) {
			$x = $this->x;

			$q = "//Season[@fallYear='".$season."']/Skiers/Skier[@userName='".$userName."']//Distance";
			$res = $x->query($q);
			$sum = 0;
			foreach ($res as $item) {
				$sum += (int)$item->textContent;
			}
			//echo $sum;

			return $sum;
		}
	}

	class DBModel  {
		protected $db = null;

		const HOST = "127.0.0.1";
		const DBNAME = "assignment5";
		const USER = "root";
		const PWD = "";
		const OPTS = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
		const CONNSTRING = "mysql:host=" . self::HOST . ";dbname=" . self::DBNAME;

		public function __construct() {
			try {
				$this->db = new PDO(self::CONNSTRING, self::USER, self::PWD, self::OPTS);
			} catch (Exception $e) {
				die("<p style='color:red;'>Can't connect to database: " . $e->getMessage() . "</p>");
			}
		}

		public function wipe() {
			$stmt = $this->db->prepare("
				DELETE FROM club;
				DELETE FROM city;
				DELETE FROM seasonentry;
				DELETE FROM skier;"
			);
			$stmt->execute();
		}

		public function insertCity($cityName, $county) {
			$stmt = $this->db->prepare("
				INSERT INTO city(city, county) 
				VALUES (:city, :county)"
			);
			$stmt->bindValue(":city", $cityName);
			$stmt->bindValue(":county", $county);
			$stmt->execute();
		}

		public function insertClub($clubId, $clubName, $cityName) {
			$stmt = $this->db->prepare("
				INSERT INTO club(id, name, city) 
				VALUES (:id, :name, :city)"
			);
			$stmt->bindValue(":id", $clubId);
			$stmt->bindValue(":name", $clubName);
			$stmt->bindValue(":city", $cityName);
			$stmt->execute();
		}

		public function insertSkier($userName, $firstName, $lastName, $yearOfBirth) {
			$stmt = $this->db->prepare("
				INSERT INTO skier(userName, firstName, lastName, yearOfBirth) 
				VALUES (:userName, :firstName, :lastName, :yearOfBirth)"
			);
			$stmt->bindValue(":userName", $userName);
			$stmt->bindValue(":firstName", $firstName);
			$stmt->bindValue(":lastName", $lastName);
			$stmt->bindValue(":yearOfBirth", $yearOfBirth);
			$stmt->execute();
		}

		public function insertSeasonEntry($season, $userName, $totalDistance, $club) {
			$stmt = $this->db->prepare("
				INSERT INTO seasonentry(season, userName, totalDistance, club) 
				VALUES (:season, :userName, :totalDistance, :club)"
			);
			$stmt->bindValue(":season", $season);
			$stmt->bindValue(":userName", $userName);
			$stmt->bindValue(":totalDistance", $totalDistance);
			$stmt->bindValue(":club", $club);
			$stmt->execute();
		}
	}

	$log = new SkierLog();
	$model = new DBModel();
	$model->wipe(); // DEBUG

	$skiers = $log->getSkiers();
	foreach ($skiers as $skier) {
		$model->insertSkier($skier["userName"], $skier["firstName"], $skier["lastName"], (int)$skier["yearOfBirth"]);
	}

	$clubs = $log->getClubs();
	foreach ($clubs as $club) {
		$model->insertCity($club["city"], $club["county"]);
		$model->insertClub($club["id"], $club["name"], $club["city"]);
	}

	$seasons = array("2015", "2016");
	$skiers = $log->getSkiers();
	foreach ($skiers as $skier) {
		foreach ($seasons as $season) {
			$totDistance = $log->getTotalDistance($season, $skier["userName"]);
			if ($totDistance > 0) {
				$clubId = $log->getClubFromUsername($season, $skier["userName"]);
				$model->insertSeasonEntry($season, $skier["userName"], $totDistance, $clubId);
			}
		}
	}

?>
</body>
</html>